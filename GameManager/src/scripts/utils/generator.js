/**
 * Created by dusanov on 05/02/16.
 */
import {Employee} from './../models/employeeModel';
import {Skill} from './../models/skillModel';
import {Task} from './../models/taskModel';

const EMPLOYEE_CHANCE = 11;
const SKILL_CHANGE= 37;


function generateItem() {
    let rand = random();
    if (rand % EMPLOYEE_CHANCE === 0) {
        return generate(Employee);
    }
    else if (rand % SKILL_CHANGE === 0 ) {
        return generate(Skill);
    }
    else {
        return generate(Task)
    }
}



function generate(model) {
    return model.generate();
}

function random(){
    return Math.floor(Math.random() * 10000);
}

function randomRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export {generateItem, randomRange};
