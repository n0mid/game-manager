import {Skill} from './models/skillModel';
import {Employee} from './models/employeeModel';
import {Task} from './models/taskModel';

import {generateItem} from "./utils/generator";

var step = 0;
var employees = [];
var items = [];
var e=0, s=0, t = 0;
function nextStep() {
    var obj = generateItem();

    // info

    if (obj instanceof Skill) {
        s++;
    } else if (obj instanceof Employee) {
        e++;
    } else if (obj instanceof Task) {
        t++;
    }
    console.log(obj.toString());
}

for(var i=0; i< 1000; i++) {
    nextStep();
}
console.log(`Skill: ${s}`);
console.log(`Employy: ${e}`);
console.log(`Task: ${t}`);