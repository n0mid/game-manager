/**
 * Created by dusanov on 05/02/16.
 */
class Skill {
    constructor(type, time){
        this.type = type;
        this.time  = time;

    };

    toString() {
        return `Skill. Type - ${this.type}. Time -  ${this.time}.`;
    }

    static generate() {
        return new Skill(1, 4);
    }
}

export   {Skill};