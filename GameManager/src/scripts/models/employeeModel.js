/**
 * Created by dusanov on 05/02/16.
 */
class Employee {
    constructor(skills, profile){
        this.skills = skills;
        this.salary  = 0;
        this.satisfaction = 100;
        this.profile = profile;
    };

    toString() {
        return `Employee. Skills - ${this.skills}. Salary -  ${this.salary}. Satisfaction - ${this.satisfaction}. Profile ${this.profile}`;
    }

    static generate() {
        return new Employee(20, 10);
    }
}

export  {Employee};