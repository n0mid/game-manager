/**
 * Created by dusanov on 05/02/16.
 */

import {randomRange} from './../utils/generator';

class Task {
    constructor(type, deadline, complexy, important){
        this.type = type;
        this.deadline = deadline;
        this.complexity = complexy;
        this.importance = important;
    };

    toString() {
        return `Task. Type - ${this.type}. Deadline -  ${this.deadline}. Сomplexity - ${this.complexity}. Importance ${this.importance}`;
    }

    static generate() {
        let type = randomRange(1, 3),
            deadline = randomRange(1, 5),
            complexity = randomRange(1, 3),
            importance = randomRange(1, 10);

        return new Task(type, deadline, complexity, importance);
    }
}

export {Task};